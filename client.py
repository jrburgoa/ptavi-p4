
import socket

# Constantes. Dirección IP del servidor y contenido a enviar
SERVER = 'localhost'
PORT = 6001
LINE = 'Hola'


def main():
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            print(f"Enviando a {SERVER}:{PORT}:", LINE)
            my_socket.sendto(LINE.encode('utf-8'), (SERVER, PORT))

            # Recibir la respuesta inicial del servidor
            data = my_socket.recv(1024)
            print('Recibido del servidor:', data.decode('utf-8'))

            # Enviar la respuesta recibida al servidor
            my_socket.sendto(data, (SERVER, PORT))

            # Recibir la respuesta final del servidor
            data = my_socket.recv(1024)
            print('Recibido del servidor:', data.decode('utf-8'))

        print("Cliente terminado.")
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
