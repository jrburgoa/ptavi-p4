
import socket
import sys


def main():
    # Constantes. Dirección IP del servidor y contenido a enviar

    server_ip = sys.argv[1]
    server_port = int(sys.argv[2])
    REGISTER = sys.argv[3]
    message = sys.argv[4]
    expire = int(sys.argv[5])
    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            PETITION = REGISTER.upper() + " sip:" + message + " SIP/2.0\r\n" + "Expires: " + str(expire) + "\r\n\r\n"
            print(PETITION)
            my_socket.sendto(PETITION.encode('utf-8'), (server_ip, server_port))  # enviamos mensaje
            data = my_socket.recv(1024)
            print(data.decode('utf-8'))
        print("Cliente terminado.")
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
