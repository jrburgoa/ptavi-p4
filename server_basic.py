import socketserver
import sys

class EchoHandler(socketserver.BaseRequestHandler):
    """
    Echo server class
    """

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        data = self.request[0]
        sock = self.request[1]
        received = data.decode('utf-8')

        # Mostrar la dirección IP, el puerto y el mensaje recibido
        client_ip, client_port = self.client_address
        print(f"{client_ip} {client_port} {received}")

        # Convertir a mayúsculas y enviar la respuesta al cliente
        response = received.upper()
        sock.sendto(response.encode('utf-8'), self.client_address)
        print(f"Respuesta enviada al cliente: {response}")


def main():
    # Verificar los argumentos de línea de comandos
    if len(sys.argv) != 2:
        print("Uso: python server_basic.py <puerto>")
        return

    # Obtener el valor del puerto
    port = int(sys.argv[1])

    # Configurar y lanzar el servidor
    try:
        serv = socketserver.UDPServer(('', port), EchoHandler)
        print(f"Server listening in port {port}...")
        serv.serve_forever()
    except OSError as e:
        sys.exit(f"Error empezando a escuchar: {e.args[1]}.")

if __name__ == "__main__":
    main()
