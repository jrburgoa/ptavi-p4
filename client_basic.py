
import socket
import sys


def main():

    # Obtener los valores de los argumentos
    server_ip = sys.argv[1]
    server_port = int(sys.argv[2])
    message = ' '.join(sys.argv[3:])

    # Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
    try:
        with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
            print(f"Enviando a {server_ip}:{server_port}:", message)
            my_socket.sendto(message.encode('utf-8'), (server_ip, server_port))

            # Recibir la respuesta inicial del servidor
            data = my_socket.recv(1024)
            print('Recibido del servidor:', data.decode('utf-8'))

            # Enviar la respuesta recibida al servidor
            my_socket.sendto(data, (server_ip, server_port))

            # Recibir la respuesta final del servidor
            data = my_socket.recv(1024)
            print('Recibido del servidor:', data.decode('utf-8'))

            print("Cliente terminado.")
    except ConnectionRefusedError:
        print("Error conectando a servidor")


if __name__ == "__main__":
    main()
